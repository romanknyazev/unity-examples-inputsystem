v0.1.0 (2019-12-26)
- Added: Five different button interactions: Hold, MultiTap, Press (& release), SlowTap, and Tap.
