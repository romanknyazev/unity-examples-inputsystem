﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

namespace Saucy.Examples {
  public abstract class Base : MonoBehaviour {
    [SerializeField] protected Color colorNormal = Color.white;
    [SerializeField] protected Color colorSuccess = new Color(0.13f, 0.62f, 0.35f, 1f);
    [SerializeField] protected Color colorError = new Color(0.62f, 0.15f, 0.13f, 1f);
    [SerializeField] protected Image image;

    protected Inputs inputs;

    protected virtual void Awake () {
      inputs = new Inputs();
    }

    protected abstract void OnEnable ();
    protected abstract void OnDisable ();
    protected virtual void Update () { }
    protected abstract void OnInput (InputAction.CallbackContext _context);
  }
}
