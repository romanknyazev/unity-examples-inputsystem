﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Saucy.Examples {
  public class MultiTap : Base {
    [SerializeField] private float resetTime = 0.2f;
    [Tooltip("Set here and in InputActions, because you can't access the value through code at all (yet).")] [SerializeField] private int maxTaps = 2;

    public UnityEvent OnStarted;
    public UnityEvent OnPerformed;
    public UnityEvent OnCanceled;

    private int currentTaps;

    protected override void Awake () {
      base.Awake();

      image.fillAmount = 0f;
      currentTaps = 0;
    }

    protected override void OnEnable () {
      inputs.UI.MultiTap.Enable();
      inputs.UI.MultiTap.started += context => OnInput(context);
      inputs.UI.MultiTap.performed += context => OnInput(context);
      inputs.UI.MultiTap.canceled += context => OnInput(context);
    }

    protected override void OnDisable () {
      inputs.UI.MultiTap.Disable();
      inputs.UI.MultiTap.started -= context => OnInput(context);
      inputs.UI.MultiTap.performed -= context => OnInput(context);
      inputs.UI.MultiTap.canceled -= context => OnInput(context);
    }

    protected override void OnInput (InputAction.CallbackContext _context) {
      if (_context.started) {
        Debug.Log("multitap started");
        ++currentTaps;
        image.fillAmount = Mathf.Clamp01(currentTaps / maxTaps);
        image.color = colorError;
        OnStarted?.Invoke();
      }

      if (_context.performed) {
        Debug.Log("multitap performed");
        currentTaps = maxTaps;
        image.fillAmount = 1f;
        image.color = colorSuccess;
        OnPerformed?.Invoke();
        StopAllCoroutines();
        StartCoroutine(MultiTapReset(resetTime + 0.3f));
      }

      if (_context.canceled) {
        Debug.Log("multitap canceled");
        OnCanceled?.Invoke();
        StopAllCoroutines();
        StartCoroutine(MultiTapReset());
      }
    }

    private IEnumerator MultiTapReset (float _delay = 0f) {
      yield return new WaitForSeconds(_delay);

      currentTaps = 0;
      image.fillAmount = 0f;
      image.color = colorNormal;
    }
  }
}
