﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Saucy.Utilities;

namespace Saucy.Examples {
  public class SlowTap : Base {
    [SerializeField] private UIFader fader;
    [SerializeField] private float fadeSpeed = 2f;
    [Tooltip("Set here and in InputActions, because you can't access the value through code at all (yet).")] [SerializeField] private float minTapDuration = 0.5f;

    public UnityEvent OnStarted;
    public UnityEvent OnPerformed;
    public UnityEvent OnCanceled;

    private bool isPressing;
    private float currentTime;

    protected override void OnEnable () {
      inputs.UI.SlowTap.Enable();
      inputs.UI.SlowTap.started += context => OnInput(context);
      inputs.UI.SlowTap.performed += context => OnInput(context);
      inputs.UI.SlowTap.canceled += context => OnInput(context);
    }

    protected override void OnDisable () {
      inputs.UI.SlowTap.Disable();
      inputs.UI.SlowTap.started -= context => OnInput(context);
      inputs.UI.SlowTap.performed -= context => OnInput(context);
      inputs.UI.SlowTap.canceled -= context => OnInput(context);
    }

    protected override void Update () {
      base.Update();

      if (isPressing) {
        currentTime += Time.deltaTime;
        image.fillAmount = Mathf.Clamp01(currentTime / minTapDuration);
      }
    }

    protected override void OnInput (InputAction.CallbackContext _context) {

      if (_context.started) {
        Debug.Log("slowtap started");
        StopAllCoroutines();
        isPressing = true;
        currentTime = 0f;
        image.fillAmount = 0f;
        image.color = colorNormal;
        OnStarted?.Invoke();
      }

      if (_context.performed) {
        Debug.Log("slowtap performed");
        isPressing = false;
        image.fillAmount = 1f;
        image.color = colorSuccess;
        OnPerformed?.Invoke();
        StopAllCoroutines();
        StartCoroutine(SlowTapFadeOut());
      }

      if (_context.canceled) {
        Debug.Log("slowtap canceled");
        isPressing = false;
        image.color = colorError;
        OnCanceled?.Invoke();
        StopAllCoroutines();
        StartCoroutine(SlowTapFadeOut());
      }
    }

    private IEnumerator SlowTapFadeOut () {
      fader.fadeSpeed = fadeSpeed;

      yield return fader.FadeOut();

      image.fillAmount = 0f;
    }
  }
}
