﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

// NOTE: Input "canceled" isn't being used at all.

namespace Saucy.Examples {
  public class Press : Base {
    public UnityEvent OnPerformed;
    public UnityEvent OnReleased; // Still operates under "Performed" input.

    private bool isPressing;

    protected override void OnEnable () {
      inputs.UI.Press.Enable();
      inputs.UI.Press.started += context => OnInput(context);
      inputs.UI.Press.performed += context => OnInput(context);
    }

    protected override void OnDisable () {
      inputs.UI.Press.Disable();
      inputs.UI.Press.started -= context => OnInput(context);
      inputs.UI.Press.performed -= context => OnInput(context);
    }

    protected override void OnInput (InputAction.CallbackContext _context) {
      if (_context.performed) {
        isPressing = !isPressing;

        if (isPressing) {
          Debug.Log("press performed pressed");
          image.color = colorSuccess;
          OnPerformed?.Invoke();
        } else {
          Debug.Log("press performed released");
          image.color = colorNormal;
          OnReleased?.Invoke();
        }
      }
    }
  }
}
