﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Saucy.Utilities;

namespace Saucy.Examples {
  public class Hold : Base {
    [SerializeField] private UIFader fader;
    [SerializeField] private float fadeSpeed = 2f;
    [Tooltip("Set here and in InputActions, because you can't access the value through code at all (yet).")] [SerializeField] private float maxTime = 1f;

    public UnityEvent OnStarted;
    public UnityEvent OnPerformed;
    public UnityEvent OnCanceled;

    private bool isPressing;
    private float currentTime;

    protected override void Awake () {
      base.Awake();

      image.fillAmount = 0f;
    }

    protected override void OnEnable () {
      inputs.UI.Hold.Enable();
      inputs.UI.Hold.started += context => OnInput(context);
      inputs.UI.Hold.performed += context => OnInput(context);
      inputs.UI.Hold.canceled += context => OnInput(context);
    }

    protected override void OnDisable () {
      inputs.UI.Hold.Disable();
      inputs.UI.Hold.started -= context => OnInput(context);
      inputs.UI.Hold.performed -= context => OnInput(context);
      inputs.UI.Hold.canceled -= context => OnInput(context);
    }

    protected override void Update () {
      base.Update();

      if (isPressing) {
        currentTime += Time.deltaTime;
        image.fillAmount = Mathf.Clamp01(currentTime / maxTime);
      }
    }

    protected override void OnInput (InputAction.CallbackContext _context) {
      if (_context.started) {
        Debug.Log("hold started");
        StopAllCoroutines();
        isPressing = true;
        currentTime = 0f;
        image.fillAmount = 0f;
        image.color = colorNormal;
        OnStarted?.Invoke();
      }

      if (_context.performed) {
        Debug.Log("hold performed");
        isPressing = false;
        image.fillAmount = 1f;
        image.color = colorSuccess;
        OnPerformed?.Invoke();
        StopAllCoroutines();
        StartCoroutine(HoldFadeOut());
      }

      if (_context.canceled) {
        Debug.Log("hold canceled");
        isPressing = false;
        image.color = colorError;
        OnCanceled?.Invoke();
        StopAllCoroutines();
        StartCoroutine(HoldFadeOut());
      }
    }

    private IEnumerator HoldFadeOut () {
      fader.fadeSpeed = fadeSpeed;

      yield return fader.FadeOut();

      currentTime = 0f;
      image.fillAmount = 0f;
    }
  }
}
