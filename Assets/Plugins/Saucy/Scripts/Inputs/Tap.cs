﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Saucy.Utilities;

namespace Saucy.Examples {
  public class Tap : Base {
    [SerializeField] private UIFader fader;
    [SerializeField] private float fadeSpeed = 2f;
    [Tooltip("Set here and in InputActions, because you can't access the value through code at all (yet).")] [SerializeField] private float maxTapDuration = 0.5f;

    public UnityEvent OnStarted;
    public UnityEvent OnPerformed;
    public UnityEvent OnCanceled;

    private bool isPressing;
    private float currentTime;

    protected override void OnEnable () {
      inputs.UI.Tap.Enable();
      inputs.UI.Tap.started += context => OnInput(context);
      inputs.UI.Tap.performed += context => OnInput(context);
      inputs.UI.Tap.canceled += context => OnInput(context);
    }

    protected override void OnDisable () {
      inputs.UI.Tap.Disable();
      inputs.UI.Tap.started -= context => OnInput(context);
      inputs.UI.Tap.performed -= context => OnInput(context);
      inputs.UI.Tap.canceled -= context => OnInput(context);
    }

    protected override void Update () {
      base.Update();

      if (isPressing) {
        currentTime += Time.deltaTime;
        image.fillAmount = Mathf.Clamp01(currentTime / maxTapDuration);
      }
    }

    protected override void OnInput (InputAction.CallbackContext _context) {

      if (_context.started) {
        Debug.Log("tap started");
        StopAllCoroutines();
        isPressing = true;
        currentTime = 0f;
        image.fillAmount = 0f;
        image.color = colorNormal;
        OnStarted?.Invoke();
      }

      if (_context.performed) {
        Debug.Log("tap performed");
        isPressing = false;
        image.color = colorSuccess;
        OnPerformed?.Invoke();
        StopAllCoroutines();
        StartCoroutine(TapFadeOut());
      }

      if (_context.canceled) {
        Debug.Log("tap canceled");
        isPressing = false;
        image.fillAmount = 1f;
        image.color = colorError;
        OnCanceled?.Invoke();
        StopAllCoroutines();
        StartCoroutine(TapFadeOut());
      }
    }

    private IEnumerator TapFadeOut () {
      fader.fadeSpeed = fadeSpeed;

      yield return fader.FadeOut();

      currentTime = 0f;
      image.fillAmount = 0f;
    }
  }
}
